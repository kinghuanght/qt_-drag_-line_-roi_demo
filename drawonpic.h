﻿#ifndef DRAWONPIC_H
#define DRAWONPIC_H

#include "util.h"

class DrawOnPic:public QLabel
{
public:
    explicit DrawOnPic(QWidget *parent = nullptr);

    void setNowDrawing(int a);
    void saveDraws();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *);
    void checkNowSelect(QPoint Pos);
    QVector<QPoint> m_zhixing_lines;
    QVector<QPoint> m_tingzhi_lines;
    QVector<QPoint> m_zuozhuan_lines;
    QVector<QPoint> m_youzhuan_lines;
    QVector<QPoint> m_paishi_area1;
    QVector<QPoint> m_paishi_area2;
    QVector<QPoint> m_paishi_area3;
    QVector<QPoint> m_paishi_area4;
    QVector<QPoint> m_paishi_area5;
    QVector<QPoint> m_recg_roi;

private:
    int m_nowDrawing;
    int m_nowMoving;
    QPen m_penPoint;
    QPen m_penLine;
    QPen m_penText;
    QImage *m_img;
};

#endif // DRAWONPIC_H
