﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "util.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT



public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();



private slots:
    void on_pushButton_zhixing_clicked();

    void on_pushButton_paishi_1_clicked();

    void on_pushButton_tingzhi_clicked();

    void on_pushButton_zuozhuan_clicked();

    void on_pushButton_youzhuan_clicked();

    void on_pushButton_paishi_2_clicked();

    void on_pushButton_paishi_3_clicked();

    void on_pushButton_paishi_4_clicked();

    void on_pushButton_paishi_5_clicked();

    void on_pushButton_save_draw_clicked();

    void on_pushButton_ROI_clicked();

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
