﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qpoint.h>
#include "qevent.h"
#include "qdebug.h"
#include <QPainter>
#include "dragpushbutton.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_zhixing_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_ZHI_XING_LINE);
}

void MainWindow::on_pushButton_tingzhi_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_TING_ZHI_LINE);
}

void MainWindow::on_pushButton_zuozhuan_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_ZUO_ZHUAN_LINE);
}

void MainWindow::on_pushButton_youzhuan_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_YOU_ZHUAN_LINE);
}

void MainWindow::on_pushButton_paishi_1_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_PAI_SHI_AREA_1);
}

void MainWindow::on_pushButton_paishi_2_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_PAI_SHI_AREA_2);
}

void MainWindow::on_pushButton_paishi_3_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_PAI_SHI_AREA_3);
}

void MainWindow::on_pushButton_paishi_4_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_PAI_SHI_AREA_4);
}

void MainWindow::on_pushButton_paishi_5_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_PAI_SHI_AREA_5);
}

void MainWindow::on_pushButton_save_draw_clicked()
{
    ui->label_pic->saveDraws();
}

void MainWindow::on_pushButton_ROI_clicked()
{
    ui->label_pic->setNowDrawing(DRAWING_RECG_ROI);
}
