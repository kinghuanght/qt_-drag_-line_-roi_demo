﻿#include "drawonpic.h"

#define MOVING_ENABLE_DISTANCE (5)//使能移动的最小距离
#define PAISHI_AREA_MAX_POINTS  (8)//牌识区域做多有多少个点

DrawOnPic::DrawOnPic(QWidget *parent) : QLabel(parent)
{
    m_penPoint.setWidth(10);
    m_penPoint.setColor(Qt::red);

    m_penLine.setWidth(3);
    m_penLine.setColor(Qt::red);

    m_penText.setWidth(6);
    m_penText.setColor(Qt::red);

    QImage *img=new QImage;
    m_img=new QImage;
    img->load("test.jpg");
    *m_img=img->scaled(1024,540,Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    this->setMouseTracking(true);
}

void DrawOnPic::saveDraws()
{

}

void DrawOnPic::setNowDrawing(int a)
{
    m_nowDrawing = a;
    switch(m_nowDrawing)
    {
        case DRAWING_ZHI_XING_LINE://清空直行线
            m_zhixing_lines.clear();
            break;
        case DRAWING_TING_ZHI_LINE://清空停止线
            m_tingzhi_lines.clear();
            break;
        case DRAWING_ZUO_ZHUAN_LINE://清空左转线
            m_zuozhuan_lines.clear();
            break;
        case DRAWING_YOU_ZHUAN_LINE://清空右转线
            m_youzhuan_lines.clear();
            break;
        case DRAWING_PAI_SHI_AREA_1://清空牌识区域1
            m_paishi_area1.clear();
            break;
        case DRAWING_PAI_SHI_AREA_2://清空牌识区域2
            m_paishi_area2.clear();
            break;
        case DRAWING_PAI_SHI_AREA_3://清空牌识区域3
            m_paishi_area3.clear();
            break;
        case DRAWING_PAI_SHI_AREA_4://清空牌识区域4
            m_paishi_area4.clear();
            break;
        case DRAWING_PAI_SHI_AREA_5://清空牌识区域5
            m_paishi_area5.clear();
            break;
        case DRAWING_RECG_ROI://清空识别roi
            m_recg_roi.clear();
            break;
        default:
            break;
    }

    update();
}

void DrawOnPic::checkNowSelect(QPoint Pos)
{
    int i;
    //检查直行线的端点
    for(i = 0;i<m_zhixing_lines.size();i++)
    {
        QPoint a = Pos-m_zhixing_lines[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from zhi xing"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_ZHI_XING_LINE_P1+i;
            return;
        }
    }

    //检查停止线的端点
    for(i = 0;i<m_tingzhi_lines.size();i++)
    {
        QPoint a = Pos-m_tingzhi_lines[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from zhi xing"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_TING_ZHI_LINE_P1+i;
            return;
        }
    }

    //检查左转线的端点
    for(i = 0;i<m_zuozhuan_lines.size();i++)
    {
        QPoint a = Pos-m_zuozhuan_lines[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from zhi xing"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_ZUO_ZHUAN_LINE_P1+i;
            return;
        }
    }

    //检查右转线的端点
    for(i = 0;i<m_youzhuan_lines.size();i++)
    {
        QPoint a = Pos-m_youzhuan_lines[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from zhi xing"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_YOU_ZHUAN_LINE_P1+i;
            return;
        }
    }

    //检查牌识区域1的端点
    for(i = 0;i<m_paishi_area1.size();i++)
    {
        QPoint a = Pos-m_paishi_area1[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_PAI_SHI_AREA_1_P1+i;
            return;
        }
    }

    //检查牌识区域1的端点
    for(i = 0;i<m_paishi_area2.size();i++)
    {
        QPoint a = Pos-m_paishi_area2[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_PAI_SHI_AREA_2_P1+i;
            return;
        }
    }

    //检查牌识区域1的端点
    for(i = 0;i<m_paishi_area3.size();i++)
    {
        QPoint a = Pos-m_paishi_area3[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_PAI_SHI_AREA_3_P1+i;
            return;
        }
    }

    //检查牌识区域1的端点
    for(i = 0;i<m_paishi_area4.size();i++)
    {
        QPoint a = Pos-m_paishi_area4[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_PAI_SHI_AREA_4_P1+i;
            return;
        }
    }

    //检查牌识区域5的端点
    for(i = 0;i<m_paishi_area5.size();i++)
    {
        QPoint a = Pos-m_paishi_area5[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_PAI_SHI_AREA_5_P1+i;
            return;
        }
    }

    //检查roi的端点
    for(i = 0;i<m_recg_roi.size();i++)
    {
        QPoint a = Pos-m_recg_roi[i];
        double b = sqrt(a.x()*a.x()+a.y()*a.y());
        //qDebug()<<"from pai shi 1,"<<i<<":"<<b;
        if(b<=MOVING_ENABLE_DISTANCE)
        {
            m_nowMoving = MOVING_RECG_ROI_P1+i;
            return;
        }
    }
}

void DrawOnPic::mousePressEvent(QMouseEvent *event)
{
    // 如果是鼠标左键按下,检查是不是在某个点附近
    if(event->button() == Qt::LeftButton)
    {
        checkNowSelect(event->pos());
    }
    // 如果是鼠标右键按下，
    else if(event->button() == Qt::RightButton)
    {
        m_nowDrawing = DRAWING_END;
    }
    update();
}

void DrawOnPic::mouseMoveEvent(QMouseEvent *event)
{

    if(m_nowMoving>=MOVING_ZHI_XING_LINE_P1
            && m_nowMoving<=MOVING_ZHI_XING_LINE_P2)//移动直行线端点
    {
        m_zhixing_lines[m_nowMoving-MOVING_ZHI_XING_LINE_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_TING_ZHI_LINE_P1
            && m_nowMoving<=MOVING_TING_ZHI_LINE_P2)//移动停止线端点
    {
        m_tingzhi_lines[m_nowMoving-MOVING_TING_ZHI_LINE_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_ZUO_ZHUAN_LINE_P1
            && m_nowMoving<=MOVING_ZUO_ZHUAN_LINE_P2)//移动左转线端点
    {
        m_zuozhuan_lines[m_nowMoving-MOVING_ZUO_ZHUAN_LINE_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_YOU_ZHUAN_LINE_P1
            && m_nowMoving<=MOVING_YOU_ZHUAN_LINE_P2)//移动右转线端点
    {
        m_youzhuan_lines[m_nowMoving-MOVING_YOU_ZHUAN_LINE_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_PAI_SHI_AREA_1_P1
            && m_nowMoving<=MOVING_PAI_SHI_AREA_1_P8)//移动牌识区域1端点
    {
        m_paishi_area1[m_nowMoving-MOVING_PAI_SHI_AREA_1_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_PAI_SHI_AREA_2_P1
            && m_nowMoving<=MOVING_PAI_SHI_AREA_2_P8)//移动牌识区域2端点
    {
        m_paishi_area2[m_nowMoving-MOVING_PAI_SHI_AREA_2_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_PAI_SHI_AREA_3_P1
            && m_nowMoving<=MOVING_PAI_SHI_AREA_3_P8)//移动牌识区域3端点
    {
        m_paishi_area3[m_nowMoving-MOVING_PAI_SHI_AREA_3_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_PAI_SHI_AREA_4_P1
            && m_nowMoving<=MOVING_PAI_SHI_AREA_4_P8)//移动牌识区域4端点
    {
        m_paishi_area4[m_nowMoving-MOVING_PAI_SHI_AREA_4_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_PAI_SHI_AREA_5_P1
            && m_nowMoving<=MOVING_PAI_SHI_AREA_5_P8)//移动牌识区域5端点
    {
        m_paishi_area5[m_nowMoving-MOVING_PAI_SHI_AREA_5_P1] = event->pos();
        update();
    }
    else if(m_nowMoving>=MOVING_RECG_ROI_P1
            && m_nowMoving<=MOVING_RECG_ROI_P8)//移动牌识区域5端点
    {
        m_recg_roi[m_nowMoving-MOVING_RECG_ROI_P1] = event->pos();
        update();
    }

}

void DrawOnPic::mouseReleaseEvent(QMouseEvent *event)
{
    m_nowMoving = MOVING_BEGIN;
    qDebug()<<"now drawing is:"<<m_nowDrawing;

    //记录直行线端点
    if((m_nowDrawing == DRAWING_ZHI_XING_LINE)
            &&(m_zhixing_lines.size()<2))
    {
        m_zhixing_lines.append(event->pos());
    }

    //记录停止线端点
    if((m_nowDrawing == DRAWING_TING_ZHI_LINE)
            &&(m_tingzhi_lines.size()<2))
    {
        m_tingzhi_lines.append(event->pos());
    }

    //记录左转线端点
    if((m_nowDrawing == DRAWING_ZUO_ZHUAN_LINE)
            &&(m_zuozhuan_lines.size()<2))
    {
        m_zuozhuan_lines.append(event->pos());
    }

    //记录右转线端点
    if((m_nowDrawing == DRAWING_YOU_ZHUAN_LINE)
            &&(m_youzhuan_lines.size()<2))
    {
        m_youzhuan_lines.append(event->pos());
    }

    //记录牌识区域1端点
    if((m_nowDrawing == DRAWING_PAI_SHI_AREA_1)
            &&(m_paishi_area1.size()<8))
    {
        m_paishi_area1.append(event->pos());
    }

    //记录牌识区域2端点
    if((m_nowDrawing == DRAWING_PAI_SHI_AREA_2)
            &&(m_paishi_area2.size()<8))
    {
        m_paishi_area2.append(event->pos());
    }

    //记录牌识区域3端点
    if((m_nowDrawing == DRAWING_PAI_SHI_AREA_3)
            &&(m_paishi_area3.size()<8))
    {
        m_paishi_area3.append(event->pos());
    }

    //记录牌识区域4端点
    if((m_nowDrawing == DRAWING_PAI_SHI_AREA_4)
            &&(m_paishi_area4.size()<8))
    {
        m_paishi_area4.append(event->pos());
    }

    //记录牌识区域5端点
    if((m_nowDrawing == DRAWING_PAI_SHI_AREA_5)
            &&(m_paishi_area5.size()<8))
    {
        m_paishi_area5.append(event->pos());
    }

    //记录牌识区域5端点
    if((m_nowDrawing == DRAWING_RECG_ROI)
            &&(m_recg_roi.size()<8))
    {
        m_recg_roi.append(event->pos());
    }

    update();
}

void DrawOnPic::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //绘制图片
    painter.drawImage(QPoint(0,0), *m_img);

    int cx,cy;
    int i = 0;
    int tmpsize;
    QPoint points[8];

    //绘制直行线
    for(int i = 0;i<m_zhixing_lines.size();i++)
    {
        qDebug()<<"zhixing"<<m_zhixing_lines[i];
        painter.setPen(m_penPoint);
        painter.drawPoint(m_zhixing_lines[i]);
        if(i == 1)//画满两个点了，就结束
        {
            painter.setPen(m_penLine);
            painter.drawLine(m_zhixing_lines[0], m_zhixing_lines[1]);
            cx = (m_zhixing_lines[0].x()+m_zhixing_lines[1].x())/2;
            cy = (m_zhixing_lines[0].y()+m_zhixing_lines[1].y())/2;
            painter.setPen(m_penText);
            painter.drawText(cx-9,cy,QStringLiteral("直行线"));
            if(m_nowDrawing == DRAWING_ZHI_XING_LINE)
                m_nowDrawing = DRAWING_END;
        }
    }

    //绘制停止线
    for(int i = 0;i<m_tingzhi_lines.size();i++)
    {
        qDebug()<<"tingzhi"<<m_tingzhi_lines[i];
        painter.setPen(m_penPoint);
        painter.drawPoint(m_tingzhi_lines[i]);
        if(i == 1)//画满两个点了，就结束
        {
            painter.setPen(m_penLine);
            painter.drawLine(m_tingzhi_lines[0], m_tingzhi_lines[1]);
            cx = (m_tingzhi_lines[0].x()+m_tingzhi_lines[1].x())/2;
            cy = (m_tingzhi_lines[0].y()+m_tingzhi_lines[1].y())/2;
            painter.setPen(m_penText);
            painter.drawText(cx-9,cy,QStringLiteral("停止线"));
            if(m_nowDrawing == DRAWING_TING_ZHI_LINE)
                m_nowDrawing = DRAWING_END;
        }
    }

    //绘制左转线
    for(int i = 0;i<m_zuozhuan_lines.size();i++)
    {
        qDebug()<<"zuozhuan"<<m_zuozhuan_lines[i];
        painter.setPen(m_penPoint);
        painter.drawPoint(m_zuozhuan_lines[i]);
        if(i == 1)//画满两个点了，就结束
        {
            painter.setPen(m_penLine);
            painter.drawLine(m_zuozhuan_lines[0], m_zuozhuan_lines[1]);
            cx = (m_zuozhuan_lines[0].x()+m_zuozhuan_lines[1].x())/2;
            cy = (m_zuozhuan_lines[0].y()+m_zuozhuan_lines[1].y())/2;
            painter.setPen(m_penText);
            painter.drawText(cx-9,cy,QStringLiteral("左转线"));
            if(m_nowDrawing == DRAWING_ZUO_ZHUAN_LINE)
                m_nowDrawing = DRAWING_END;
        }
    }

    //绘制右转线
    for(int i = 0;i<m_youzhuan_lines.size();i++)
    {
        qDebug()<<"youzhuan"<<m_youzhuan_lines[i];
        painter.setPen(m_penPoint);
        painter.drawPoint(m_youzhuan_lines[i]);
        if(i == 1)//画满两个点了，就结束
        {
            painter.setPen(m_penLine);
            painter.drawLine(m_youzhuan_lines[0], m_youzhuan_lines[1]);
            cx = (m_youzhuan_lines[0].x()+m_youzhuan_lines[1].x())/2;
            cy = (m_youzhuan_lines[0].y()+m_youzhuan_lines[1].y())/2;
            painter.setPen(m_penText);
            painter.drawText(cx-9,cy,QStringLiteral("右转线"));
            if(m_nowDrawing == DRAWING_YOU_ZHUAN_LINE)
                m_nowDrawing = DRAWING_END;
        }
    }

    //绘制牌识区域1
    tmpsize = m_paishi_area1.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_paishi_area1[i]);
        points[i] = m_paishi_area1[i];
        cx += m_paishi_area1[i].x();
        cy += m_paishi_area1[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_PAI_SHI_AREA_1)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_paishi_area1[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx-13,cy,QStringLiteral("牌识区域1"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }

    //绘制牌识区域2
    tmpsize = m_paishi_area2.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_paishi_area2[i]);
        points[i] = m_paishi_area2[i];
        cx += m_paishi_area2[i].x();
        cy += m_paishi_area2[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_PAI_SHI_AREA_2)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_paishi_area2[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx,cy,QStringLiteral("牌识区域2"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }

    //绘制牌识区域3
    tmpsize = m_paishi_area3.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_paishi_area3[i]);
        points[i] = m_paishi_area3[i];
        cx += m_paishi_area3[i].x();
        cy += m_paishi_area3[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_PAI_SHI_AREA_3)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_paishi_area3[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx-13,cy,QStringLiteral("牌识区域3"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }

    //绘制牌识区域4
    tmpsize = m_paishi_area4.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_paishi_area4[i]);
        points[i] = m_paishi_area4[i];
        cx += m_paishi_area4[i].x();
        cy += m_paishi_area4[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_PAI_SHI_AREA_4)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_paishi_area4[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx-13,cy,QStringLiteral("牌识区域4"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }

    //绘制牌识区域5
    tmpsize = m_paishi_area5.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_paishi_area5[i]);
        points[i] = m_paishi_area5[i];
        cx += m_paishi_area5[i].x();
        cy += m_paishi_area5[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_PAI_SHI_AREA_5)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_paishi_area5[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx-13,cy,QStringLiteral("牌识区域5"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }

    //绘制识别ROI
    tmpsize = m_recg_roi.size();
    cx = cy = 0;
    painter.setPen(m_penPoint);
    for(i = 0;i<tmpsize;i++)
    {
        painter.drawPoint(m_recg_roi[i]);
        points[i] = m_recg_roi[i];
        cx += m_recg_roi[i].x();
        cy += m_recg_roi[i].y();
        if(i == 7 && m_nowDrawing == DRAWING_RECG_ROI)//画满8个点了，就结束
            m_nowDrawing = DRAWING_END;
    }
    if(tmpsize>0)
    {
        //不满8个点，则用第一个点的坐标填满剩余，用于封闭
        for(int j = i;j<8;j++)
        {
            points[j] = m_recg_roi[0];
        }
        painter.setPen(m_penText);
        cx = cx/tmpsize;
        cy = cy/tmpsize;
        painter.drawText(cx-13,cy,QStringLiteral("ROI"));
        painter.setPen(m_penLine);
        painter.drawPolygon(points,8);
    }
}
